import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  //  Class Variables
  final String title = 'Event Slate';
  static const int appMainColor = 0xFF016252;
  Map<int, Color> color = {
    50:Color.fromRGBO(136,14,79, .1),
    100:Color.fromRGBO(136,14,79, .2),
    200:Color.fromRGBO(136,14,79, .3),
    300:Color.fromRGBO(136,14,79, .4),
    400:Color.fromRGBO(136,14,79, .5),
    500:Color.fromRGBO(136,14,79, .6),
    600:Color.fromRGBO(136,14,79, .7),
    700:Color.fromRGBO(136,14,79, .8),
    800:Color.fromRGBO(136,14,79, .9),
    900:Color.fromRGBO(136,14,79, 1),
  };

  @override
  Widget build(BuildContext context) {

    MaterialColor materialCustomColor = MaterialColor(0xFF016252, color);

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: materialCustomColor,
      ),
      home: MyHomePage(title: title, appMainColor: appMainColor),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;
  final int appMainColor;

  MyHomePage({Key key, @required this.title, this.appMainColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Color(appMainColor),
      body: Center(
        child: Image.asset('images/logo2.png'),
      ),
    );
  }
}
